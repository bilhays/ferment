#!/bin/bash
#

#####
# Installation script that installs a launchd file
#####
version=1.1;
RUN_TIME=`date "+%y%m%d%H%M%S"`;                     # Time stamp
PROGRAM=${0##*/};                                    # Set the name of this script
NO_ARGS=0 ;                                          # Process arguments for getopts
E_OPTERROR=65;                                       # Process arguments for getopts
EXTRN_FUNCTIONS="./_functions.sh"    # This requires a set of functions kept outside this file
TARGET_DIR="/Library/LaunchDaemons/";
TEMPLATE_DIR="./launchd/";
TEMPLATE_FILE='';

# Include the functions file
if [[ -f ${EXTRN_FUNCTIONS} ]]
   then 
   source ${EXTRN_FUNCTIONS};
else
   echo "Sorry, can't find the ${EXTRN_FUNCTIONS} file, dying here....":
   exit 5; # use exit directly here since we don't have functions
fi

function usage()
   {
echo "   
   OPTIONS:
      -h   Show this message
      -t   The template file to use
      -f   Force an overwrite of the file
      -v   Version
   
   All launch daemons will be set run at a random time between
   1 and 6 am to distribute network load.
   
   Current possble templates are:";

   pushd ${TEMPLATE_DIR} > /dev/null;
   ls -1 edu*  
   popd > /dev/null;
   
   } # end of usage()
   
function unload_launchdaemon()
   {
   ERROR=0; 
   echo "   Stopping stop ${TEMPLATE_FILE%.plist}";
   launchctl stop ${TEMPLATE_FILE%.plist};
      if [[ ${?} != 0 ]]
         then
         echo "   Error stopping ${TEMPLATE_FILE%.plist}";
         ERROR=1;
      fi
   echo "   Unloading ${TARGET_DIR}${TEMPLATE_FILE}";
   launchctl unload ${TARGET_DIR}${TEMPLATE_FILE};
      if [[ ${?} != 0 ]]
         then
         echo "   Error unloading ${TEMPLATE_FILE%.plist}";
         ERROR=1;
      fi

   return ${ERROR};
   }
   


# Make sure we're in the program's own directory, the path references require this
if [[ ${0} != "./${PROGRAM}" ]];
   then 
   error "Sorry, no soup for you here, you need to run this from it's home directory."
   cleanup_and_exit 10;
fi;

# test for superuser, need to be that
if [ `id -u` -ne 0 ];
then
   error "Only superuser permitted to run this script";
   cleanup_and_exit 15;
fi;  

if [[ ! ${1}  ]];
   then
   usage;
   exit 1;
fi;

while getopts "ht:f" Option
do
  case $Option in
    h     ) usage;;
    t     ) echo;
            echo "   Template file is ${OPTARG}";
    		TEMPLATE_FILE=${OPTARG};;
    f     ) echo "   Forcing overwrite of existing files";
            FORCE="yes";;
    v	  ) echo "version is ${version}";;
    *     ) 
          echo "Unimplemented option chosen.";
          usage;
          ;;   
          # DEFAULT
        
   esac
done

######
# Sanity Checks

if [[ ! ${TEMPLATE_FILE} ]]
   then 
   echo;
   echo "No template file specified";
   usage;
   exit 2;
fi

if [[ ! -f ${TEMPLATE_DIR}${TEMPLATE_FILE} ]] 
   then
   echo;
   echo "   ${TEMPLATE_FILE} does not exist";
   usage;
   exit 3;
   fi

if [[ -f ${TARGET_DIR}${TEMPLATE_FILE} && ${FORCE} != "yes" ]]
   then
   echo "   ${TARGET_DIR}${TEMPLATE} exists, are you sure";
   echo "   you want to replace it with this new version?";
   echo -n "   [y|N]";
   read ans
   if [[ ${ans} == "y" ]]
      then
      echo "   Ok, we'll proceed....";
      cp ${TARGET_DIR}${TEMPLATE_FILE} \
         "./backups/${TEMPLATE_FILE}_${RUN_TIME}";
      if [[ ${?} == 0 ]]
         then
         echo "   ./backups/${TEMPLATE_FILE}_${RUN_TIME} \
            is the backup copy";
         echo "   Removing ${TARGET_DIR}${TEMPLATE_FILE}";
         unload_launchdaemon;
         rm ${TARGET_DIR}${TEMPLATE_FILE};
      else
         echo "   Failed to make a backup of ${TARGET_DIR}${TEMPLATE}, aborting";
      fi   
   else
      echo "   Ok, we're aborting....";   
      cleanup_and_exit 1;
      
   fi
fi


minute=$(date "+%S");
hour=7;

while [[ ${hour}<1 || ${hour}>6 ]]; 
   do 
   #echo ${hour}; 
   let hour=$RANDOM%10; 
done;

#exit

sed s/HOUR/${hour}/g < ${TEMPLATE_DIR}${TEMPLATE_FILE} \
   | sed s/MINUTE/${minute}/g \
   > ${TARGET_DIR}${TEMPLATE_FILE}; 

if [[ ${FORCE} == "yes" ]]
  then
  echo
   x=$(launchctl list | grep edu.unc.cs.clamcron);
   if [[ ${x} ]]
      then
      unload_launchdaemon;
      fi
   fi
echo "   Loading ${TARGET_DIR}${TEMPLATE_FILE}";
launchctl load ${TARGET_DIR}${TEMPLATE_FILE};   
echo "   Starting ${TEMPLATE_FILE%.plist}";
launchctl start ${TEMPLATE_FILE%.plist};

exit 0;