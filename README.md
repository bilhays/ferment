Mac Installation notes

Logic

The idea is to automate as much as possible mac installations. Since the OS configures itself to hardware on boot, we might be able to handle that, but not yet. So start with a machine that has the os and has been patched. This is basically a container--clone it down

There are multiple classes of installations:
*Installers from .pkg files
*Binary Copies of applications
*Makes and make installs
*Other installers, like VISE

At this point in history, we're only dealing with the first two, esp. given brew and brew cask.



There are also configuration files to copy over. To do that we use ditto so as to preserve permissions and ownership, as well as any macness to the files. 

Applications that can be copied can just be copied. So there's an applications folder and anything you put there will be copied into applications. Utilities work the same way, just put them in the utilities folder in the applications folder. Rsync is what we use for this, and it makes a backup. 

The bin directory holds binary files for installers that aren't packages. To use this, make a directory for the thing you want to install, and reproduce the tree for the installation. For example, in the Kerberos folder, you'll find all of the binary files in a replica tree. The installation system will ditto that tree onto the target.

The config directory works the same way, but for configuration files and scripts (we lump scripts into the configuration since some scripts are used for configuration, and they are text files).

Functionally, there's no difference in how bin and config are treated, but I thought made since 15+ years ago to keep them separate.

The packages directory contains any package files that are used in installations.

The patches directory contains any patches that should be run against the installation.

Some General Guidelines:

-Before running on a volume that you did not boot from, check the drive's info in finder to make sure that the ignore permissions flag is not set. If it's set, permissions on the folders and files will not be set correctly. This is a common setting on removable media (eg. firewire drives).

-Packages and applications can be installed as root, but be careful with config files and binaries if you use brew, you want to be consistent in /usr/local/. One good thing to think about are setting permissions and sanity checks in the postflight script.

-This stuff is for me very old code, and pretty crufty, but it still works ok for me.
