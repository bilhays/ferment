#!/bin/bash
#

#####
# Installation suite that installs a package and
# a standard set of configuration files
#####
version="1.3";

#####
# Set user options
#####
APP_NAME="c"                   # Name of the Application or Config suite to install
                                     # Note that this is carried to find things in the tree
PACKAGE="";               # Name of the package to install, if any
INSTALL_CONF="install.conf"
PACKAGE_DIR="packages";              # Directory for storing os x package files
CONFIG_DIR="config";                 # Directory for storing configuration files and scripts
BIN_DIR="bin";                          # Directory for storing binary and other static files
PATCH_DIR="patches";                        # Directory for storing any patch files to be applied

# find the owner of /usr/local, that's our admin users
if [[ -d /usr/local/ ]]
   then
   URSLOCALOWNER=$(ls -l /usr/local | awk '{print $3}');

fi

#####
# Function: Usage
# Tells the user how the program expects to run
# Returns zero on success
# No input arguments
function usage()
   {
cat <<EOF

Usage: ${PROGRAM} [OPTIONS] TARGET_DIRECTORY
   -v = Echo the version number
   -b = Install binaries and packages (eg. upgrade)
   -c = Install config files and scripts
   -p Install patches
   -s Install source code
   -a Install all (This should work in all cases
   for a fresh installation, and possibly all generic cases
   echo;
   The most common usage would be:
   ${PROGRAM} -a /

EOF
exit;
    }


#####
# Set system options (you shouldn't need to change these)
#####

EXTRN_FUNCTIONS="./_functions.sh";    # This requires a set of functions kept outside this file
MAIN_SH="./_main.sh";                 # A common main body to keep things simple
PREFLIGHT="./_preflight.sh";
POSTFLIGHT="./_postflight.sh";

# Include the functions file
if [[ -f ${EXTRN_FUNCTIONS} ]]
then
   source ${EXTRN_FUNCTIONS};
else
   echo "Sorry, can't find the ${EXTRN_FUNCTIONS} file, dying here....":
   exit 5; # use exit directly here since we don't have functions
fi

# find the owner of /usr/local, that's our admin users
if [[ -d /usr/local/ ]]
   then
   URSLOCALOWNER=$(ls -l /usr/local | awk '{print $3}');
else
   echo "WARNING: no /usr/local/, we will likely fail!"
   exit 4;
fi


# Evaulate the command line
# Defaults to do nothing
install_patches_flag=no;
install_package_flag=no;
install_config_flag=no;
install_source_flag=no;
while getopts ":vbcpsa" Option
do
  case $Option in
    v     ) echo;
            echo "   Version is ${version}";
    		echo;
            exit;;
    b     ) echo "   Install binaries and packages (eg. upgrade)";
    		install_package_flag=yes;;
    c     ) echo "   Install config files and scripts";
    		install_config_flag=yes;;
    p     ) echo "   Install patches";
    		install_patches_flag=yes;;
    s     ) echo "   Install source code";
    	    install_source_flag=yes;;
    a     ) echo "   Install all";
    		install_patches_flag=yes;
    		install_package_flag=yes;
    		install_config_flag=yes;
    		install_source_flag=yes;;
    *     )
          echo "Unimplemented option chosen.";
          usage;
          ;;   # DEFAULT
   esac
done

# Include the preflight file
if [[ -f ${PREFLIGHT} ]]
then
   echo "Sourcing PREFLIGHT";
   source ${PREFLIGHT};
   if [[ ${0} !=0 ]]
      then
      echo "Sorry, can't source the ${PREFLIGHT} file, dying here....":
      exit 5; # use exit directly here since we may not have functions
   fi
fi

# Include the main file
if [[ -f ${MAIN_SH} ]]
then
   source ${MAIN_SH};
else
   echo "Sorry, can't find the ${MAIN_SH} file, dying here....":
   exit 6; # use exit directly here since we may not have functions
fi

# Include the postflight file
if [[ -f ${POSTFLIGHT} ]]
then
   echo "Sourcing POSTFLIGHT";
   source ${POSTFLIGHT};
   if [[ ${0} !=0 ]]
      then
      echo "Sorry, can't source the ${POSTFLIGHT} file, dying here....":
      exit 7; # use exit directly here since we may not have functions
fi

# Clean up
echo;
echo "Cleaning up";
# Remove temp directory
if [[ -d ${TMPDIR} ]]
   then
   echo "   Removing tmp dir: ${TMPDIR}";
   rmdir ${TMPDIR};
   if [[ ${?} -ne 0 ]]
      then
      error "   Unable to remove tmp directory ${TMPDIR}";
     cleanup_and_exit 20;
   fi
fi

# clean up .DS_Store files
find ./ -name '.DS_Store' -exec rm '{}' +;

fin;



