#####
# Main program begins here
#
# If everything is set up correctly, you shouldn't need to touch
# any of this from here on
#####
version="1.3";
APPLICATIONS_DIR="Applications";                     # Where the applications are stored
APPLICATIONS_BACKUP="${APPLICATIONS_DIR}/Installer_Backup_Folder";  # Where to put copies of applications
GREP="/usr/bin/grep";
RSYNC="/usr/bin/rsync";                              # Which rsync to use, and flags
INSTALLER_APP="/usr/sbin/installer";                 # The standard package installer for OS X
DITTO_APP="/usr/bin/ditto";                          # Ditto is used to copy files preserving macness and rights
RSYNC="/usr/bin/rsync";
RUN_TIME=`date "+%y-%m-%d-%H.%M.%S"`;                     # Time stamp
TMPDIR="tmp${RUN_TIME}";                             # Set a directory name for any tmp files
NO_ARGS=0 ;                                          # Process arguments for getopts
E_OPTERROR=65;                                       # Process arguments for getopts
NEEDTOBESUPER=yes;                                   # You should be superuser to run this

PROG="${0##*/}";
#PROG="${PROG%.new}";
#PROG="${PROG%.sh}";
#PROG="${PROG%.bash}";


#####
# Function: Usage
# Tells the user how the program expects to run
# Returns zero on success
# No input arguments

function usage()
   {
cat <<EOF
Usage: ${PROGRAM} [OPTIONS] TARGET_DIRECTORY
   -b = Install binaries and packages (eg. upgrade)
   -c = Install config files and scripts
   -p Install patches
   -s Install source code
   -a Install all (This should work in all cases
   for a fresh installation, and possibly all generic cases
   echo;
   The most common usage would be:
   ${PROGRAM} -a /
   echo;
EOF
exit;



#####
# Sanity Checks
#####


# Make sure we're in the program's own directory, the path references require this
if [[ ${0} != "./${PROGRAM}" ]]
then
   error "Sorry, no soup for you here, you need to run this from it's home directory."
   usage;
   cleanup_and_exit 10;
fi

if [ ${NEEDTOBESUPER:-""} == "yes" -a `id -u` -ne 0 ];
then
   error "Only superuser permitted to run this script";
   usage;
   cleanup_and_exit 15;
fi;

# This says what to do when the user hits CTRL-C, CTRL-\ or we receive a SIGTERM
trap "cleanup_and_exit" 1 2 3 13 14 15;

# Determine where I am....
#echo ${0}
case ${0} in
    ./*)
        DIR=${PWD}/${0#./};
        DIR=$(dirname "${DIR}");
        # put a sanity check here
        #echo "case 1 $DIR";
        ;;
    *)
        DIR=$(dirname "${0}");
        #echo "case 2 $DIR";
        ;;
esac;



echo "****************************************";
echo "   Beginning $PROGRAM at $RUN_TIME";
echo "";

# Make a temp directory
echo "   Making tmp dir: ${TMPDIR}";
mkdir ${TMPDIR};
if [[ ${?} -ne 0 ]]
then
   error "Unable to make tmp directory: ${TMPDIR}";
   cleanup_and_exit 20;
fi



# Decrement the argument pointer so it points to next argument.
# so that $1  references the first non option item
# supplied on the command line (if one exists).
shift $(($OPTIND - 1));

# Set target
# echo ${1};
target_dir=${1};
if [[ ${1} == "" ]]
then
  error "You must set a target directory";
  usage;
  cleanup_and_exit 25;
fi

# Install the packages
if [[ ${install_package_flag} == "yes" ]]
then
   install_package;
   copy_binary;
   copy_applications;
fi


#echo "install_config_flag is ${install_config_flag}";
if [[ ${install_config_flag} == "yes" ]]
then
   # Check for configuration files
   check_config_file;
   # Copy standard configs over
   copy_config;
   echo "    copy_config_result is ${copy_config_result}";
fi

if [[ ${install_patches_flag} == "yes" ]]
   then
   echo;
   echo "Checking for files to patch";
   if [[ ${PATCH_DIR} == '' ]];
	  then
  	  echo "   PATCH_DIR not specified, apparently there's nothing to patch";
	  patch_files_result=1;
	  #echo "1 ${patch_files_result}";
   else
      # Make patches
      echo "Applying patches from ${PATCH_DIR} ";
      patch_files;
      result=${?};

      echo "Patch_files_result is ${result}";
      if [ ${result} -eq "1" ]
         then
      echo "Patching failed to complete successfully, probably due to lack of patches or dir mismatch";
      elif [ ${result} -eq "0" ]
         then
         echo "Looks like all patches were successful";
      else
         echo "*****WARNING: ${result} attempted patch(es) failed to complete successfully";
      fi
   fi
fi


# when building from source, specify the directory
# where the source files are stored, and the program
# to run if there is a particular one.
#if [[ ${install_source_flag} == "yes" ]]
if [[ ${install_source_flag} == "yes" ]]
   then
   echo;
   echo "Checking for source files to build";
      if [[ ${SOURCE_INSTALLER} && ${SOURCE_DIR} ]];
         then
         pushd "${DIR}/source/${SOURCE_DIR}"
         echo "   Running ${DIR}/source/${SOURCE_DIR}/${SOURCE_INSTALLER}";
         ./${SOURCE_INSTALLER};
         if [[ ${?} -ne 0 ]];
            then
            error "Installer program apparently failed";
            cleanup_and_exit 19;
         fi
         popd;
      else
         echo "   SOURCE_INSTALLER or SOURCE_DIR not specified, apparently there's";
         echo "   no source to build here";
      fi
fi


