#####
# Functions
#
version="1.2f"
# Much of this work done by Murray Anderegg,
# thanks to him for letting me steal his code
#
# 1.2c added support for spaces in package names.
# 1.2d added make_source, make_install, and untar
# 1.2e added function add_paths
# 1.2f bug fixes
#####
# Function name: stderr
# Purpose of function: Echoes its input parameters on stderr
# Returns: 0 (success)
# Input Arguments:
stderr () {
	echo "$@" 1>&2;
	return 0;
}

#####
# Function name: error
# Purpose of function: calls stderr with the string "Error: " followed by
#                      the command-line arguments
# Returns: 0 (success)
# Input Arguments:
error () {
	stderr Error: "$@";
	return 0;
}

#####
# Function name: cleanup
#
# Purpose of function: It cleans up any of our work before exiting
# Returns: Function would return true(0) if temp file is not empty
# Input Arguments: <none>
cleanup ()
   {
   if [ -e ${TMPDIR} ];
   then
      /bin/rm -fR ${TMPDIR};
      fi;
   }

#####
# Function name: cleanup_and_exit
# Purpose of function: Run cleanup and exit with the error code given
#                      1 is default exit code
# Returns: <function does not return>
# Input Arguments: integer exit code
# Notes:
cleanup_and_exit ()
{
    EXITCODE=${1:-1};
    cleanup;
    exit ${EXITCODE};
}


#####
# Function name: ask
# Purpose of function: Prompt the user to answer a question
# Returns: 0 normally.
#          non-zero if the user closed stdin.
#          The string of the answer is echoed to stdout. The answer is
#          the user reply. If the user just hits <enter>, then the default
#          is returned. If no default is supplied, then return an empty string.
# Input Arguments: 1 - The prompt question for the user
#                  2 (optional) - Default answer if the user hits <enter>
# Notes:
# A default answer can be given as the second parameter
# Example:
# ask "How old are you" "21"
#
# Gives:
# How old are you [21] ?
ask () {
    # Pull the prompt as the first command-line parameter
    prompt="$1";
    shift;
    # Get the default answer, if there is one.
    if [ $# -gt 0 ];
    then
        default="$1";
        shift;
    else
        default="";
    fi;
    # Prompt the user for an answer and then read the answer into ans.
    #echo_no_nl "$prompt [$default] ? " >&2;
    echo -n  "$prompt [$default] ? " >&2;

    read ans;
    status=$?;
    # Check to see if the user responded with anything.
    # If yes, then return what the user responded.
    # If not, then return the default answer.
    case $ans in
        ?*)
            echo "$ans";
            ;;
        *)
            echo "$default";
            ;;
    esac;
    # Return whether we successfully read an answer from the user.
    # This is non-zero if the user closes stdin.
    return $status;
} # end of function #

####
# Function name: askyes
# Purpose of function: Ask the user a yes/no question that defaults to a yes
#                      answer
# Returns: 0 (for a yes answer) or 1 (for a non-yes answer)
# Input Arguments: 1 - Question to ask user
# Notes:
askyes () {
  ans=`ask "$1" "y"`;
  # Check whether the answer begins with a 'Y' or 'y'
  # Return success if answer matched a 'Y' or 'y'
  case $ans in
    [Yy]*)
      return 0;
      ;;
    *)
      return 1;
      ;;
  esac;
} # end of function #

#####
# Function name: askno
# Purpose of function: Ask the user a yes/no question that defaults to a no
#                      answer
# Returns: 0 (for a no answer) or 1 (for a non-no answer)
# Input Arguments: 1 - Question to ask user
# Notes:
askno () {
  ans=`ask "$1" "N"`;
  # Check whether the answer begins with a 'N' or 'n'
  # Return success if answer matched a 'N' or 'n'
  case $ans in
    [Nn]*)
      return 0;
      ;;
    *)
      return 1;
      ;;
  esac;
}

#####
# Function name: debug
# Purpose of function: Output a debugging message if debugging is turned on
# Returns: 0, success
# Input Arguments:
#    $1 = debug level
#    $@ = debug message
# Notes:
debug () {
    case ${DEBUG} in
	[Yy]*)
	    if [ "$1" -ge ${DEBUGLEVEL} ];
	    then
		shift;
		stderr "$@";
	    fi;
	    ;;
	*)
	    ;;
    esac;
}


#####
# This checks for an error and exits with a custom message
# Returns zero on success
# $1 is the message
# $2 is the error code

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi
   }

#####
# This checks for an error and exits with a custom message
# Returns zero on success
# $1 is the message
# $2 is the error code

function check_err
   {
   if  [[ ${?} != "0" ]]
      then
      echo "   WARNING: we failed.";
      exit 233;
      fi
   }


#####
# This checks for the existance of a required file
function check_file
   {
   if [[ -f ${1} ]]
      then
	  echo "${1} exists, proceeding...."
   else
      echo "${1} not present, we're dying here...."
	  exit 10;
   fi
   }

#####
# Functions name: fin
#####
# Purpose of function: display reasonable message at the end of a run
# Returns text
# No input args
function fin () {

   echo "   Fini avec $PROGRAM";
   echo "****************************************";
   echo " ";
   cleanup_and_exit 0;
   }

#####
# Function setup_sudoers.d
#####
# This checks for the presence of a /etc/sudoers.d dir
# and the directive enabling same.
function setup_sudoers.d
{
   if [[ ! -d "/etc/sudoers.d" ]]
      then
      mkdir  "/etc/sudoers.d";
   fi

   if [[ ! $(grep "#includedir /etc/sudoers.d" "/etc/sudoers") ]]
      then
      echo "#includedir /etc/sudoers.d" >> "/etc/sudoers"
   fi
}

function config_source
   {
   echo "   Configuring using ${CONFIGFLAGS}";
   ./configure ${CONFIGFLAGS};
   check_err;
   }

function make_source
   {
   make clean;
   check_err;
   make;
   check_err;
   }

function make_install
   {
   make install;
   check_err;
   }

function untar
   {
   tar -xvf ${1}
   check_err;
   }

######
#
# add_paths takes a string and, if that line isn't in .profile,
# adds a line that prepends that string to PATH
# string must end in :
# beware sudo! Use sudo -i so you'll be at root's home
# $1 is the path to add, $2 is the user id
#
function add_paths
   {
   echo "   2 is $2";
   if [[ ${2} && ${2} != root ]]
      then
      PROFILE="/Users/${2}/.profile";
   elif [[ ${2} == root ]]
      then
      PROFILE="/var/root/.profile";
   else
      echo "***No user specified";
   fi

   if [[ ! -f ${PROFILE} ]]
     then
     echo "   No .profile, touching....";
     touch ${PROFILE};
   fi
   if [[ $(${GREP} ${1} ${PROFILE}) ]]
      then
      echo "   ${1} is in .profile already";
   else
      echo "export PATH=${1}"'${PATH}' >> ${PROFILE} || check_errm "***Warning: Failed to add ${1} to ${PROFILE}" 40;
      export PATH="${1}${PATH}";
   fi

   if [[ ${2} && ${2} != "root" ]]
      then
      chown ${2}:staff "/Users/${2}/.profile";
   fi

   }

####### Everything below is specific to the installer suite

#####
# Function: check_dirs
# Check for dirs we need on os x systems and make them
# if they aren't in place
# pass it a list of dirs
function check_dir
   {
   if [[ ! -d ${1} ]]
      then
      mkdir ${1}
      if [[ ${?} == 0 ]]
         then
         echo "   Made ${1}";
         return 0;
      else
         echo "   Failed to make ${1}";
         return 2;
      fi
   else
      echo "   ${1} exists, moving on.";
      return 0;
   fi
   }


#####
# Function: check_config_file
# This checks for configuration files that are already in place
# in the target directory
# To get the list of files, it reads the directory tree in the source config
# directory. This means that the tree structure in the source config dir
# _must_ reflect the relative locations of the config files--which is
# also required for our use of the ditto command
# No input arguments
function check_config_file
{
   #echo "DIR is $DIR";
   #echo "config_dir is $CONFIG_DIR"
   if [[ -d ${DIR}/${CONFIG_DIR} ]]
      then
      echo;
      echo "Checking for configuration files and scripts already in place:";
      # First, strip the source config dir of .DS_Store files
      find ./${CONFIG_DIR} -type f -name .DS_Store -delete;

      echo "    Checking for alised files in /";
      if [[ -d "${DIR}/${CONFIG_DIR}/etc" || -d "${DIR}/${CONFIG_DIR}/tmp" || -d "${DIR}/${CONFIG_DIR}/var" ]]
         then
         echo "***FATAL ERROR: /etc /var/ and /tmp are softlinked";
         echo "   Check ${DIR}/${CONFIG_DIR} for these dirs";
         echo "   Use /private instead";
         let "copy_config_result += 1";
         exit;
      fi

      # Fill an array with the files in the source config dir
      # Then make timestamped backups if the same files exist at the target
      pushd ${DIR}/${CONFIG_DIR} > /dev/null;           #Move to the config soure dir so the find will be cleaner
      declare -a config_array=`find . -type f`;         #Declare an array and file all regular files in the config dir
      for config_file_instance in ${config_array}       #Loop through and make backups in the target dir of any files
      #                                                  #found in the source dir
      do
         if [ -f ${target_dir}/${config_file_instance} ]
         then
            echo "   ${config_file_instance##*/} exists..."
            #echo cp -f -p ${target_dir}/${config_file_instance} ${target_dir}/${config_file_instance}.${RUN_TIME};
            cp -f -p ${target_dir}/${config_file_instance} ${target_dir}/${config_file_instance}.${RUN_TIME};
         fi
      done
      popd > /dev/null;
   else
      echo "   No config dir specified or";
      echo "   ${DIR}/${CONFIG_DIR}";
      echo "   doesn't exist, moving along";

   fi
}

#####
# Function: copy_config
# This rsync's the config files from the source tree to the target tree
# No input arguments
function copy_config
{
# Need a check err and return here
if [[ ${CONFIG_DIR} ]]
   then
   copy_config_result=0;
   echo "   Copying configuration files and scripts using:";
   echo "   ${RSYNC} -rv "${DIR}/${CONFIG_DIR}/*" ${target_dir}";
   ${RSYNC} -rv "${DIR}/${CONFIG_DIR}/" ${target_dir};

   copy_config_attempt=${?};
   if [ ${copy_config_attempt} -eq "0" ]
      then
      echo "   Attempt to rsync ${DIR}/${CONFIG_DIR} apparently successful";
      else
      echo "***WARNING: Attempt to rsync ";
      echo "${DIR}/${CONFIG_DIR} apparently failed";
      let "copy_config_result += 1";
   fi
else
   echo "   No configuration files or scripts to install or CONFIG_DIR not specified.";
fi
}

#####
# Function: copy_applications
# Dittos applications from the source tree to the target tree
# Takes no arguments
function copy_applications
{
if [[ ${APPLICATIONS_DIR} ]]
   then
   echo "   Copying Applications with:";
   echo "   ${DITTO_APP} --noextattr --norsrc -V ${DIR}/${APPLICATIONS_DIR}/ ${APPLICATIONS_DIR}/";
   ${DITTO_APP} --noextattr --norsrc -V ${DIR}/${APPLICATIONS_DIR}/ /${APPLICATIONS_DIR}/;
   else
      echo "   No binary or other static support files to install or APP_DIR not specified.";
   fi
}




#####
# Function: copy_binary
# This dittos binary files from the source tree to the target tree
# No input arguments
function copy_binary
{
if [[ ${BIN_DIR} ]]
   then
   echo "   Copying binary and other static support files:";
   ${DITTO_APP} --noextattr --norsrc -V ${DIR}/${BIN_DIR} ${target_dir}/;
   else
      echo "   No binary or other static support files to install or BIN_DIR not specified.";
   fi
}

#####
# Functions: patch_files
# This patches files from the source directory to the target
#
function patch_files
   {
   patch_files_result=0;
   echo;
   echo "Checking for patches for ${APP_NAME}";


	   # First, strip the source config dir of .DS_Store files
	   find ./${PATCH_DIR} -type f -name .DS_Store -delete;
	   #find ./${PATCH_DIR} -type f;

       echo "Trying to pushd ${DIR}/${PATCH_DIR} > /dev/null";
	   pushd ${DIR}/${PATCH_DIR} > /dev/null;   #Move to the patches source dir so the find will be cleaner
       if [ ${?} -ne "0" ];
          then
          echo "*****WARNING: ${DIR}/${PATCH_DIR} doesn't pushd cleanly";
          patch_files_result=1;
       fi
	   echo declare -a patch_array=`find . -type f`;      #Declare an array and file all regular files in the config dir
	   #echo ${patch_array};

	   for patch_array_instance in ${patch_array}           #Loop through and make backups in the target dir of any files
	   #                                                    #found in the source dir
	      do
	      echo ${target_dir}/${patch_array_instance};
	      if [ -f ${target_dir}/${patch_array_instance%.*} ]
		     then
		     echo "${patch_array_instance##*/} exists, backup and patch..."
		     cp -f -p ${target_dir}/${patch_array_instance%.*} ${target_dir}/${patch_array_instance%.*}.${RUN_TIME};
		     if [[ $?==0 ]]
			    then
			    # Attempt patch
			    patch -l --verbose ${target_dir}/${patch_array_instance%.*} < ${patch_array_instance};
			    patch_attempt=${?};
			    if [ ${patch_attempt} -eq "0" ]
                   then
				   echo "${target_dir}/${patch_array_instance%.*} patched.";
				 else
				    echo "*****WARNING: Attempt to patch ${target_dir}/${patch_array_instance%.*} failed";
				    let "patch_files_result += 1";
				    echo ${patch_files_result};
				 fi
			  else
				 echo "This attempt to backup a target file failed, skipping this one:"
				 echo "cp -f -p ${target_dir}/${patch_array_instance} ${target_dir}/${patch_array_instance}.${RUN_TIME}";
		      fi
		      #cp -f -p ${target_dir}/${patch_array_instance} ${target_dir}/${patch_array_instance}.${RUN_TIME};

	          popd > /dev/null;
           fi
       done

   #echo "5 ${patch_files_result}";
   return ${patch_files_result};

}


#####
# Function: install package
# This runs the installer app to install a standard os x package.
# Be aware that how the installer runs depends alot on the package maker
function install_package
{
   install_packet_result=0;
   if [[ ${PACKAGE} ]]
   then # Install the package...
      echo "   Attempting to install ${package} to ${target_dir}:";
      echo "   Using $INSTALLER_APP -verbose -pkg $DIR/$PACKAGE_DIR/$PACKAGE -target";
      $INSTALLER_APP -verbose -pkg "$DIR/$PACKAGE_DIR/$PACKAGE" -target $target_dir;
      install_package_attempt=${?};
      if [ ${install_package_attempt} -eq "0" ]
          then
          echo "${DIR}/${PACKAGE_DIR}/${PACKAGE} Installed";
          else
          echo "*****WARNING: Attempt to install ${DIR}/${PACKAGE_DIR}/${PACKAGE} apparently failed";
             let "install_package_result += 1";
             fi
   else
      echo "   No packages to install or package not specified.";
   fi
   return ${install_package_result};
}


function check-os ()
   {
   if [[ $(uname -a | grep buntu) ]]
      then
      OS="Ubuntu";
   elif  [[ $(uname | grep Darwin) ]]
      then
      OS="Darwin";
   elif [[ -f "/etc/redhat-release" ]]
      then
      OS="Redhat";
   else
      OS="Unk";
   fi
   }

